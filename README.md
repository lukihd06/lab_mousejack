# lab_mousejack

## Scenario

Un groupe de Hacker est engagé par un état enemis dans le but de récupérer des informations sur l'armement Français. Il font alors une étude pour trouver une vulnérabilitée dans la sécurité de la DGA. Les infrastructures et la cyber sécurité étant dificillement pénétrables, ils se rabatent sur la vulnérabilitée humaine. Pour cela ils ont recours au Social Engeneering. Une fois de plus ils se heurtent à un problème de sécurité car en interne les utilisateurs sont difficilement atteignables. Mais ceux qui font des déplacement avec le pc du travail peuvent ne pas respecter les rêgles de sécurité. Après cette enquête ils trouvent un membre de la DGA qui voyage en train et utilise une souris sans fil qui detient une vulnérabilitée "keylogger". L'attaquant vas donc utiliser le matériel néscésaire pour injecter dans la souris un script permettant de récupérer les infomations sensibles détenus sur le disque dur de la victime.

## Matériels nécessaire 

Souris potentiellement vulnérables aux attaques HID :
```
Logitech M185
```

Souris non vulnérables aux attaques HID ou non reconnues :
```
AmazonBasics MG-0975
```